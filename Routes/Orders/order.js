import { Router } from "express";

import createOrder from "../../Controllers/Orders/createOrder.js";
import updateOrder from "../../Controllers/Orders/updateOrder.js";
import deleteOrder from "../../Controllers/Orders/deleteOrder.js";
import readOrder from "../../Controllers/Orders/readOrder.js";

import userAuth from "../../Middleware/Middleware_userAuth.js";
import userOder from "../../Middleware/Middleware_userOrder.js";
import deliverOrder from "../../Middleware/Middleware_deliverOrder.js";

const router = Router();

router.post("/order/create", userAuth, createOrder);
router.put("/order/update/:id", deliverOrder, updateOrder);
router.delete("/order/delete/:id", [userOder, deliverOrder], deleteOrder);
router.get("/order/read/:id", userOder, readOrder);

export default router;
