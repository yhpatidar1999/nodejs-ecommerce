import { application, Router } from "express";
import createItem from "../../Controllers/Items/createItem.js";
import updateItem from "../../Controllers/Items/updateItem.js";
import readItem from "../../Controllers/Items/readItem.js";
import deleteItem from "../../Controllers/Items/deleteItem.js";
import availableItems from "../../Controllers/Items/availableItems.js";

const router = Router();

//milddlewares
import adminAuth from "../../Middleware/Middleware_adminAuth.js";

// user acces routes
router.get("/items/read/:id", readItem);
router.get("/items/available", availableItems);


// admin authcation middleware
// router.use(adminAuth);

// admin acces routes
router.post("/items/create",adminAuth, createItem);
router.put("/items/update/:id",adminAuth, updateItem);
router.delete("/items/delete/:id",adminAuth, deleteItem);

export default router;
