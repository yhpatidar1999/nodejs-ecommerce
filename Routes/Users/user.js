import { Router } from "express";
import createUser from "../../Controllers/Users/createUser.js";
import loginUser from "../../Controllers/Users/loginUser.js";
import updateUser from "../../Controllers/Users/updateUser.js";
import deleteUser from "../../Controllers/Users/deleteUser.js";

const router = Router();

//milddlewares
import userAuth from "../../Middleware/Middleware_userAuth.js";
import adminAuth from "../../Middleware/Middleware_adminAuth.js";

// create user route
router.post('/user/create', createUser)
router.post('/user/login', loginUser)
router.put('/user/update',userAuth, updateUser)
router.delete('/user/delete/:id',adminAuth, deleteUser)


export default router;