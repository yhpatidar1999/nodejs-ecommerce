import express from 'express'
import dotenv from 'dotenv'
import userRouter from '../../Routes/Users/user.js'
import itemRouter from '../../Routes/Items/items.js'
import orderRouter from '../../Routes/Orders/order.js'

dotenv.config()

const app = express()
const PORT = process.env.PORT 

app.use(express.json()) // for parsing application/json

// users route
app.use(userRouter)
app.use(itemRouter)
app.use(orderRouter)


app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`)
})
