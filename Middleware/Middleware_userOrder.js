import db from "../Config/firebase_conf.js";
import { doc, getDoc } from "firebase/firestore";
import jwt from "jsonwebtoken";

const userOder = async (req, res, next) => {
  let orderID = req.params.id;
  const JWT_SECRET = process.env.JWT_SECRET;

  try {
    // get email from token
    let bearerToken = req.headers["authorization"];
    if (!bearerToken) {
      return res.status(401).json({
        success: false,
        message: "No Authorization token found, please login first.",
      });
    }
    let token = bearerToken.split(" ")[1];

    let decodeUser = jwt.verify(token, JWT_SECRET);
    let userEmail = decodeUser.email;

    // get email from orderID
    let orderDocRef = doc(db, "orders", orderID);
    let orderDoc = await getDoc(orderDocRef);
    if (orderDoc.exists()) {
      let orderData = orderDoc.data();
      let orderEmail = orderData.email;
      if (orderEmail == userEmail) {
        next();
      } else {
        res.status(401).json({
          success: false,
          message: "You are not authorized to access this order",
        });
      }
    } else {
      return res.status(404).json({ msg: "order not found" });
    }
  } catch (error) {
    console.log("firestore error for updating order", error);
    return res.status(500).json({ msg: "Server Error" });
  }
};

export default userOder;
