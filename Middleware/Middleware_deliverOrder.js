import { getDoc,doc } from "firebase/firestore";
import db from "../Config/firebase_conf.js";

const deliverOrder = async (req, res, next) => {
  let orderId = req.params.id;
  try {
    let orderDocRef = doc(db, "orders", orderId);
    let orderDoc = await getDoc(orderDocRef);
    let orderData = orderDoc.data();

    if (orderData.status == "Delivered") {
      return res.status(401).json({
        success: false,
        message: "Order is Delivered ,now it is not be update and delete",
      });
    } else {
      next();
    }
  } catch (e) {
    console.log("error in diliverOrder", e);
    res.status(401).json({
      success: false,
      message: "The provided jwt token is not valid",
    });
  }
};

export default deliverOrder;
