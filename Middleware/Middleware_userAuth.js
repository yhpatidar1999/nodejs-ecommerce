import jwt from "jsonwebtoken";

const userAuth = (req, res, next) => {
  const JWT_SECRET = process.env.JWT_SECRET;
  try {
    let bearerToken = req.headers["authorization"];
    
    if (!bearerToken) {
      return res.status(401).json({
        success: false,
        message: "No Authorization token found, please login first.",
      });
    }
   let token = bearerToken.split(" ")[1];

    jwt.verify(token,JWT_SECRET );
    next();
  } catch (e) {
    console.log("error in userAuth", e);
    res.status(401).json({
      success: false,
      message: "The provided jwt token is not valid",
    });
  }
};

export default userAuth;
