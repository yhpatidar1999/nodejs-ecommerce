import jwt from "jsonwebtoken";

const adminAuth = (req, res, next) => {
  const JWT_SECRET = process.env.JWT_SECRET;
  try {
    let bearerToken = req.headers["authorization"];

    if (!bearerToken) {
      return res.status(401).json({
        success: false,
        message: "No Authorization token found, please login first.",
      });
    }
    let token = bearerToken.split(" ")[1];

    let decodeUser = jwt.verify(token, JWT_SECRET);

    if (decodeUser.isAdmin) {
      next();
    } else {
      return res.status(401).json({
        message: "You are not an admin",
      });
    }
  } catch (e) {
    console.log("error in adminAuth", e);
    res.status(401).json({
      success: false,
      message: "The provided jwt token is not valid",
    });
  }
};

export default adminAuth;
