import { doc, getDoc,collection,deleteDoc } from "firebase/firestore";
import db from "../../Config/firebase_conf.js";

const deleteItem = async (req, res) => {
  const itemId = req.params.id;
  console.log(itemId);

    const itemCol = collection(db, "items");
    try {
        const itemRef = doc(itemCol, itemId);
        const itemDoc = await getDoc(itemRef);
        if (!itemDoc.exists()) {
            return res.status(400).json({ msg: "Item does not exist" });
        }
        await deleteDoc(itemRef);
        return res.status(200).json({ msg: "Item deleted" });
    } catch (error) {
        console.log("firestore error for check item and delete", error);
        return res.status(500).json({ msg: "Server Error" });
    }
};



export default deleteItem;
