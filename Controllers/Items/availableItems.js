import db from "../../Config/firebase_conf.js";
import { collection, query, getDocs, where } from "firebase/firestore";

const availableItems = async (req, res) => {
  try {
    const itemsCol = collection(db, "items");
    const itemsRef = query(itemsCol, where("stock", ">", 0));
    const itemsSnapshot = await getDocs(itemsRef);
    // const items = [];
    // itemsSnapshot.forEach((doc) => {
    //     items.push(doc.data());
    // });

    const items = itemsSnapshot.docs.map((doc) => doc.data());

    res.status(200).json(items);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

export default availableItems;
