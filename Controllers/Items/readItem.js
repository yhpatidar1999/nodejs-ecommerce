import { doc, getDoc } from "firebase/firestore";
import db from "../../Config/firebase_conf.js";

const readItem = async (req, res) => {
  const itemId = req.params.id;
  console.log(itemId);

  try {
    const itemDocRef = doc(db, "items", itemId);
    const itemDoc = await getDoc(itemDocRef);

    if (!itemDoc.exists()) {
      return res.status(400).json({ msg: "item does not exist" });
    }
    const item = itemDoc.data();
    return res.status(200).json({ msg: "item is found", item });
  } catch (error) {
    console.log("firestore error for check item", error);
    return res.status(500).json({ msg: "Server Error" });
  }
};

export default readItem;
