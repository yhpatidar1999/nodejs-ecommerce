import { addDoc, collection } from "firebase/firestore";
import db from "../../Config/firebase_conf.js";

const createItem = async (req, res) => {
  const { title, price, category, stock } = req.body;

  if (!title || !price || !category || !stock) {
    return res.status(400).json({ msg: "Please enter all fields" });
  }

  const itemCol = collection(db, "items");

  const item = {
    title,
    price,
    category,
    stock,
  };

  try {
    const itemDocRef = await addDoc(itemCol, item);
    return res
      .status(200)
      .json({ msg: "item created successfull", item: item, id: itemDocRef.id });
  } catch (error) {
    console.log("firestore error for creating user", error);
    return res.status(500).json({ msg: "Server Error" });
  }
};

export default createItem;
