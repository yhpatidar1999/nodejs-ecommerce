import { doc,updateDoc } from "firebase/firestore";
import db from '../../Config/firebase_conf.js';

const updateItem = async (req, res) => {

  const itemId = req.params.id;
  const {  title, price, category, stock } = req.body;

  if (!itemId) return res.status(400).json({ msg: "Please enter item id" });

  try {
   const itemDocRef = doc(db, "items", itemId);
    const updateItem = await updateDoc(itemDocRef, {
      price,
    });
    return res.status(200).json({ msg: "item is updated", updateItem });
  } catch (error) {
    console.log("firestore error for check user", error);
    return res.status(500).json({ msg: "Server Error" });
  }
};

export default updateItem;
