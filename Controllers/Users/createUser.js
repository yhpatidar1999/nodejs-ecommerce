import { collection, getDoc, setDoc, doc } from 'firebase/firestore';
import bcrypt from 'bcrypt';
import db from '../../Config/firebase_conf.js';


const createUser = async (req, res) => {
    const { name, email, password } = req.body;

    // check all fields are filled
    if (!name || !email || !password) {
        return res.status(400).json({ msg: 'Please enter all fields' });
    }
    const userCol  = collection(db, 'users');

    try {
        // Check for existing user in firestore
        const userRef = doc(userCol, email);
        const userDoc = await getDoc(userRef);
        if (userDoc.exists()) {
            return res.status(400).json({ msg: 'User already exists', user: userDoc.data() });
        }
    } catch (error) {
        console.log("firestore error for check user", error);
        return res.status(500).json({ msg: 'Server Error' });

    }

    // Create salt & hash
    const salt = await bcrypt.genSalt(10);
    let hash = await bcrypt.hash(password, salt);
    const newUser = {
        name,
        email,
        password: hash
    }

    try {
        // Save user to firestore
        const userRef = doc(userCol, email);
        await setDoc(userRef, newUser);
        return res.status(200).json({ msg: 'User created', user: newUser });
    } catch (error) {
        console.log("firestore error for creating user", error);
        return res.status(500).json({ msg: 'Server Error' });
    }

}

export default createUser; 