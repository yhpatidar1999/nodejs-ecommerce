import { collection, doc, getDoc, deleteDoc } from "firebase/firestore";
import db from '../../Config/firebase_conf.js';

const deleteUser = async (req, res) => {
    // delete user
    const email = req.params.id;
    // check all fields are filled
    if (!email) {
        return res.status(400).json({ msg: 'Please enter all fields' });
    }
    const userCol = collection(db, 'users');
    try {
        // Check for existing user in firestore
        const userRef = doc(userCol, email);
        const userDoc = await getDoc(userRef);
        if (!userDoc.exists()) {
            return res.status(400).json({ msg: 'User does not exist' });
        }
        // Delete user
        await deleteDoc(userRef);
        return res.status(200).json({ msg: 'User deleted' });
    } catch (error) {
        console.log("firestore error for check user and delete", error);
        return res.status(500).json({ msg: 'Server Error' });
    }
}

export default deleteUser;