import db from '../../Config/firebase_conf.js';
import bcrypt from 'bcrypt';
import { collection, getDoc, doc } from 'firebase/firestore';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
dotenv.config();

const JWT_SECRET = process.env.JWT_SECRET; // secret for jwt

const loginUser = async (req, res) => {
    const { email, password } = req.body;
    // check all fields are filled
    if (!email || !password) {
        return res.status(400).json({ msg: 'Please enter all fields' });
    }
    const userCol = collection(db, 'users');
    try {
        // Check for existing user in firestore
        const userRef = doc(userCol, email);
        const userDoc = await getDoc(userRef);
        if (!userDoc.exists()) {
            return res.status(400).json({ msg: 'User does not exist' });
        }
        // Check password
        const user = userDoc.data();
        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch) {
            return res.status(400).json({ msg: 'Invalid Password' });
        }

        // Create token
        const token = jwt.sign(user, JWT_SECRET, { expiresIn: "36h" });
        if (!token) {
            return res.status(500).json({ msg: 'Server Error(token)' });
        }
        return res.status(200).json({ msg: "user is login", token, user });
    } catch (error) {
        console.log("firestore error for check user", error);
        return res.status(500).json({ msg: 'Server Error' });
    }
}


export default loginUser;


