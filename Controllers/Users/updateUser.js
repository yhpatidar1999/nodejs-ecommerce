import db from '../../Config/firebase_conf.js';
import bcrypt from 'bcrypt';
import { collection, getDoc, doc,setDoc } from 'firebase/firestore';

const updateUser = async (req, res) => {
    // get user data from request
    const { name, email, password, newName, newPassword } = req.body;

    // check all fields are filled
    if (!name || !email || !password || !newName || !newPassword) {
        return res.status(400).json({ msg: 'Please enter all fields' });
    }


    const userCol = collection(db, 'users');
    try {
        // Check for existing user in firestore
        const userRef = doc(userCol, email);
        const userDoc = await getDoc(userRef);
        if (!userDoc.exists()) {
            return res.status(400).json({ msg: 'User does not exist' });
        }
        // Check password
        const user = userDoc.data();
        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch) {
            return res.status(400).json({ msg: 'Invalid Password' });
        }

        // Update user data
        const salt = await bcrypt.genSalt(10);
        let hash = await bcrypt.hash(newPassword, salt);
        const updatedUser = {
            name: newName,
            email,
            password: hash
        };
        await setDoc(userRef, updatedUser);
        return res.status(200).json({ msg: "user is updated", updatedUser });
    } catch (error) {
        console.log("firestore error for check user", error);
        return res.status(500).json({ msg: 'Server Error' });
    }
}

export default updateUser;


