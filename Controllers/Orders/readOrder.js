import { doc, getDoc } from "firebase/firestore";
import db from "../../Config/firebase_conf.js";

const readOrder = async (req, res) => {
  const id = req.params.id;
  console.log(id);
  if (!id) {
    return res.status(400).json({ msg: "Please give order id" });
  }
  try {
    const orderRef = doc(db, "orders", id);
    const orderDoc = await getDoc(orderRef);
    if (orderDoc.exists()) {
      return res
        .status(200)
        .json({ msg: "order found", data: orderDoc.data() });
    } else {
      return res.status(404).json({ msg: "order not found" });
    }
  } catch (error) {
    console.log("firestore error for reading order", error);
    return res.status(500).json({ msg: "Server Error" });
  }
};

export default readOrder;
