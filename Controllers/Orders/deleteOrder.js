import {doc,getDoc,deleteDoc} from "firebase/firestore";
import db from "../../Config/firebase_conf.js";

const deleteOrder =async (req, res) => {
    const id  = req.params.id;
    console.log(id);
    if (!id) {
        return res.status(400).json({ msg: "Please enter all fields" });
    }
    try {
        const orderRef = doc(db, "orders", id);
        const orderDoc = await getDoc(orderRef);
        if (orderDoc.exists()) {
            await deleteDoc(orderRef);
            return res.status(200).json({ msg: "order deleted successfull" });
        } else {
            return res.status(404).json({ msg: "order not found.." });
        }
    } catch (error) {
        console.log("firestore error for deleting order", error);
        return res.status(500).json({ msg: "Server Error" });
    }
}

export default deleteOrder;