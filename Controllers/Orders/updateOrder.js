import { getDoc, doc, updateDoc } from "firebase/firestore";
import db from "../../Config/firebase_conf.js";

const updateOrder = async (req, res) => {
  const id = req.params.id;
  const { status } = req.body;
  if (!id || !status) {
    return res.status(400).json({ msg: "Please enter all fields" });
  }
  try {
    const orderDocRef = doc(db, "orders", id);
    const orderDoc = await getDoc(orderDocRef);
    if (orderDoc.exists()) {
      await updateDoc(orderDocRef, { status });
      return res
        .status(200)
        .json({ msg: "order updated successfull", order: orderDoc.data() });
    } else {
      return res.status(404).json({ msg: "order not found" });
    }
  } catch (error) {
    console.log("firestore error for updating order", error);
    return res.status(500).json({ msg: "Server Error" });
  }
};

export default updateOrder;
