import { collection, addDoc, doc, getDoc,updateDoc } from "firebase/firestore";
import db from "../../Config/firebase_conf.js";

const createOrder = async (req, res) => {
  const { items, email } = req.body;

  if (!items || !email) {
    return res.status(400).json({ msg: "Please enter all fields" });
  }

  // first check item availability

  try {
    items.map(async (item) => {
      let itemDocRef = doc(db, "items", item.itemId);
      let itemDoc = await getDoc(itemDocRef);

      if (itemDoc.exists()) {
        if (itemDoc.data().stock < item.quantity) {
          return res.status(400).json({ msg: "Item  is out stock" });
        } else {
          const updateItem = await updateDoc(itemDocRef, {
            stock: itemDoc.data().stock - item.quantity,
          });
          console.log(updateItem);
        }
      } else {
        return res.status(400).json({ msg: "Item not exit" });
      }
    });
  } catch (error) {
    return res.status(400).json({ msg: "Item not exit" });
  }

  try {
    const orderCol = collection(db, "orders");
    const order = {
      email,
      items: items,
      status: "ordered",
    };
    const orderDocRef = await addDoc(orderCol, order);
    const orderDoc = await getDoc(orderDocRef);
    return res
      .status(200)
      .json({ msg: "order created successfull", order: orderDoc.data() });
  } catch (error) {
    console.log("firestore error for creating order", error);
    return res.status(500).json({ msg: "Server Error" });
  }
};

export default createOrder;
