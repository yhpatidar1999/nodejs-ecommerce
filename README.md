## Installation

1. Clone the repo using this command
```bash
git clone https://gitlab.com/yhpatidar1999/nodejs-ecommerce
``` 
.env file is not added 

2. Install & Run Client
   ```bash
    cd nodejs-ecommerce
    npm install
    npm run dev
    Port : http://localhost:5001
   ``` 

## User Api

1. http://localhost:5001/user/create            
2. http://localhost:5001/user/login            
3. http://localhost:5001/user/update             
4. http://localhost:5001/user/delete/:id 


##  Item Api

1. http://localhost:5001/items/create            
2. http://localhost:5001/items/update/:id
3. http://localhost:5001/items/read/:id
4. http://localhost:5001/items/available
5. http://localhost:5001/items/delete/:id


## Order Api

1. http://localhost:5001/order/create           
2. http://localhost:5001/order/update/:id          
3. http://localhost:5001/order/delete/:id          
4. http://localhost:5001/order/read/:id



